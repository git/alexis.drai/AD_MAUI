﻿using AMC.View.Resources.Strings;
using System.Globalization;

namespace AMC.View.Converters
{
    public class SongsInfoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var (songCount, totalDuration) = (ValueTuple<int, int>)value;
            var songLabel = int.Abs(songCount) < 2 ? Strings.SongsLabelSingular : Strings.SongsLabelPlural;
            var minutesLabel = int.Abs(totalDuration) < 2 ? Strings.MinutesLabelSingular : Strings.MinutesLabelPlural;
            return string.Format(
                "{0} {1}, {2} {3}",
                songCount,
                songLabel,
                totalDuration,
                minutesLabel
            );
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
