﻿using System.Globalization;

namespace AMC.View.Converters
{
    public class AlbumDetailsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var (genre, year) = (ValueTuple<string, int>)value;
            return string.Format(
                "{0} · {1}",
                genre,
                year
            );
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
