﻿using System.Globalization;

namespace AMC.View.Converters
{
    public class CopyrightInfoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var (copyrightYear, producerBlurb) = (ValueTuple<int, string>)value;
            return string.Format(
                "℗ {0} {1}",
                copyrightYear,
                producerBlurb
            );
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
