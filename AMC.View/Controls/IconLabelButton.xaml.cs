﻿namespace AMC.View.Controls
{
    public partial class IconLabelButton : ContentView
    {
        public static readonly BindableProperty ButtonSourceProperty = BindableProperty.Create(
            nameof(ButtonSource),
            typeof(string),
            typeof(IconLabelButton),
            default(string),
            propertyChanged: (bindable, oldValue, newValue) =>
            {
                ((IconLabelButton)bindable).Button.Source = (string)newValue;
            });

        public static readonly BindableProperty ButtonLabelTextProperty = BindableProperty.Create(
            nameof(ButtonLabelText),
            typeof(string),
            typeof(IconLabelButton),
            default(string),
            propertyChanged: (bindable, oldValue, newValue) =>
            {
                ((IconLabelButton)bindable).ButtonLabel.Text = (string)newValue;
            });

        public static readonly BindableProperty LabelTextColorProperty = BindableProperty.Create(
            nameof(LabelTextColor),
            typeof(Color),
            typeof(IconLabelButton),
            default(Color),
            propertyChanged: (bindable, oldValue, newValue) =>
            {
                ((IconLabelButton)bindable).ButtonLabel.TextColor = (Color)newValue;
            });

        public IconLabelButton()
        {
            InitializeComponent();
        }

        public string ButtonSource
        {
            get => (string)GetValue(ButtonSourceProperty);
            set => SetValue(ButtonSourceProperty, value);
        }

        public string ButtonLabelText
        {
            get => (string)GetValue(ButtonLabelTextProperty);
            set => SetValue(ButtonLabelTextProperty, value);
        }

        public Color LabelTextColor
        {
            get => (Color)GetValue(LabelTextColorProperty);
            set => SetValue(LabelTextColorProperty, value);
        }
    }
}
