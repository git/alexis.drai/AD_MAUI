﻿namespace AMC.View.Controls
{
    public partial class IconLabelButtonWide : ContentView
    {
        public static readonly BindableProperty ButtonSourceProperty = BindableProperty.Create(
            nameof(ButtonSource),
            typeof(string),
            typeof(IconLabelButtonWide),
            default(string),
            propertyChanged: (bindable, oldValue, newValue) =>
            {
                ((IconLabelButtonWide)bindable).Button.Source = (string)newValue;
            });

        public static readonly BindableProperty ButtonLabelTextProperty = BindableProperty.Create(
            nameof(ButtonLabelText),
            typeof(string),
            typeof(IconLabelButtonWide),
            default(string),
            propertyChanged: (bindable, oldValue, newValue) =>
            {
                ((IconLabelButtonWide)bindable).ButtonLabel.Text = (string)newValue;
            });

        public IconLabelButtonWide()
        {
            InitializeComponent();
        }

        public string ButtonSource
        {
            get => (string)GetValue(ButtonSourceProperty);
            set => SetValue(ButtonSourceProperty, value);
        }

        public string ButtonLabelText
        {
            get => (string)GetValue(ButtonLabelTextProperty);
            set => SetValue(ButtonLabelTextProperty, value);
        }

    }
}
