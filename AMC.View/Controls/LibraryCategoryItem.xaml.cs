﻿namespace AMC.View.Controls
{
    public partial class LibraryCategoryItem : ContentView
    {
        public static readonly BindableProperty CategoryTextProperty = BindableProperty.Create(
            propertyName: nameof(CategoryText),
            returnType: typeof(string),
            declaringType: typeof(LibraryCategoryItem),
            defaultValue: "",
            propertyChanged: CategoryTextChanged
            );

        public static readonly BindableProperty IconSourceProperty = BindableProperty.Create(
            propertyName: nameof(IconSource),
            returnType: typeof(ImageSource),
            declaringType: typeof(LibraryCategoryItem),
            defaultValue: null,
            propertyChanged: IconSourceChanged
            );

        public LibraryCategoryItem()
        {
            InitializeComponent();
        }

        public string CategoryText
        {
            get => (string)GetValue(CategoryTextProperty);
            set => SetValue(CategoryTextProperty, value);
        }

        public ImageSource IconSource
        {
            get => (ImageSource)GetValue(IconSourceProperty);
            set => SetValue(IconSourceProperty, value);
        }

        private static void CategoryTextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (LibraryCategoryItem)bindable;
            control.CategoryLabel.Text = (string)newValue;
        }

        private static void IconSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (LibraryCategoryItem)bindable;
            control.IconImage.Source = (ImageSource)newValue;
        }
    }

}
