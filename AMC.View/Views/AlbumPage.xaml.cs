﻿using AMC.ViewModel.ViewModels;

namespace AMC.View.Views
{
    public partial class AlbumPage : ContentPage
    {
        private readonly AlbumViewModel viewModel;

        public AlbumPage(AlbumViewModel albumViewModel)
        {
            InitializeComponent();

            viewModel = albumViewModel;
            BindingContext = viewModel;
        }
    }
}
