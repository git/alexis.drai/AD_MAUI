﻿using AMC.ViewModel.ViewModels;

namespace AMC.View.Views
{
    public partial class LibraryPage : ContentPage
    {
        private readonly LibraryViewModel viewModel;

        public LibraryPage() : this(null)
        { }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Blocker Code Smell", "S3427:Method overloads with default parameter values should not overlap ", Justification = "The parameterless ctor is needed by MAUI")]
        public LibraryPage(LibraryViewModel? libraryViewModel = null)
        {
            InitializeComponent();

            viewModel = libraryViewModel ?? new LibraryViewModel(null);
            BindingContext = viewModel;
        }

        private void OnAlbumSelected(object sender, SelectionChangedEventArgs e)
        {
            var collectionView = (CollectionView)sender;

            if (e.CurrentSelection.FirstOrDefault() is AlbumViewModel selectedAlbum)
            {
                Navigation.PushAsync(new AlbumPage(selectedAlbum));
            }

            collectionView.SelectedItem = null;
        }
    }
}
