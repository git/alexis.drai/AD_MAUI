﻿namespace AMC.Model.Models
{
    public class Album
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public string CoverImage { get; set; }
        public string Genre { get; set; }
        public int Year { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int CopyrightYear { get; set; }
        public string ProducerBlurb { get; set; }
        public List<Song> Songs { get; set; }
    }

}