﻿namespace AMC.Model.Models
{
    public class Song
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Duration { get; set; }
        public int? Index { get; set; }
    }
}
