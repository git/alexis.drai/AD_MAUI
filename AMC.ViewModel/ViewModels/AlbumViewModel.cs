﻿using AMC.Model.Models;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AMC.ViewModel.ViewModels
{
    public class AlbumViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        private readonly Album album;

        public int Id => album.Id;

        public string Title => album.Title;

        public string Artist => album.Artist;

        public string CoverImage => album.CoverImage;

        public (string Genre, int Year) Details => (album.Genre, album.Year);

        public DateTime ReleaseDate => album.ReleaseDate;

        public (int SongCount, int TotalDuration) SongsInfo
            => (album.Songs.Count, album.Songs.Sum(song => song.Duration) / 60);

        public (int CopyrightYear, string ProducerBlurb) CopyrightInfo
            => (album.CopyrightYear, album.ProducerBlurb);

        public ReadOnlyObservableCollection<SongViewModel> Songs => new(songs);

        private readonly ObservableCollection<SongViewModel> songs;


        public AlbumViewModel(Album album)
        {
            this.album = album;

            int index = 1;
            foreach (var song in this.album.Songs)
            {
                song.Index = index++;
            }

            songs = new ObservableCollection<SongViewModel>(this.album.Songs.Select(song => new SongViewModel(song)));
        }
    }
}