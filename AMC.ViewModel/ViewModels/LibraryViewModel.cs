﻿using AMC.Model.Models;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace AMC.ViewModel.ViewModels
{
    public class LibraryViewModel : INotifyCollectionChanged
    {
        public event NotifyCollectionChangedEventHandler? CollectionChanged;

        private readonly Library library;

        public ReadOnlyObservableCollection<AlbumViewModel> Albums => new(albums);

        private readonly ObservableCollection<AlbumViewModel> albums;

        public LibraryViewModel(Library? library)
        {
            this.library = library ?? new Library
            {
                Albums = new List<Album> {
                    new Album
                    {
                        Id = 1,
                        Title = "Test Album 1",
                        Artist = "Test Artist 1",
                        CoverImage = "album_macroblank_1.png",
                        Genre = "Test genre 1",
                        Year = 1970,
                        ReleaseDate = new DateTime(1970, 01, 01),
                        CopyrightYear = 1996,
                        ProducerBlurb = "Test Records Ltd",
                        Songs = new List<Song>
                        {
                            new Song { Id = 1, Title = "Test Song 1", Duration = 210 },
                            new Song { Id = 2, Title = "Test Song 2", Duration = 260 },
                            new Song { Id = 3, Title = "Test Soooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong 3", Duration = 817 },
                            new Song { Id = 4, Title = "Test Song 4", Duration = 654 },
                            new Song { Id = 5, Title = "Test Song 5", Duration = 768 },
                            new Song { Id = 6, Title = "Test Song 6", Duration = 435 },
                            new Song { Id = 7, Title = "Test Song 7", Duration = 785 },
                            new Song { Id = 8, Title = "Test Song 8", Duration = 712 },
                            new Song { Id = 9, Title = "Test Song 9", Duration = 523 },
                        }
                    },
                    new Album
                    {
                        Id = 2,
                        Title = "Test Albuuuuuuuuuuuuuuuuuuum 2",
                        Artist = "Test Artist 2",
                        CoverImage = "album_macroblank_2.png",
                        Genre = "Test genre 2",
                        Year = 1970,
                        ReleaseDate = new DateTime(1970, 01, 01),
                        CopyrightYear = 1996,
                        ProducerBlurb = "Test Records Ltd",
                        Songs = new List<Song>
                        {
                            new Song { Id = 10, Title = "Test Song 10", Duration = 456 },
                            new Song { Id = 11, Title = "Test Song 11", Duration = 83 },
                            new Song { Id = 12, Title = "Test Song 12", Duration = 4533 },
                            new Song { Id = 13, Title = "Test Song 13", Duration = 785 },
                            new Song { Id = 14, Title = "Test Song 14", Duration = 712 },
                            new Song { Id = 15, Title = "Test Song 15", Duration = 523 },
                        }
                    },
                    new Album
                    {
                        Id = 3,
                        Title = "Test Album 3",
                        Artist = "Test Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaartist 3",
                        CoverImage = "album_macroblank_3.png",
                        Genre = "Test genre 3",
                        Year = 1970,
                        ReleaseDate = new DateTime(1970, 01, 01),
                        CopyrightYear = 1996,
                        ProducerBlurb = "Test Records Ltd",
                        Songs = new List<Song>
                        {
                            new Song { Id = 16, Title = "Test Song 16", Duration = 34 },
                        }
                    },
                }
            };

            albums = new ObservableCollection<AlbumViewModel>(this.library.Albums.Select(album => new AlbumViewModel(album)));
        }
    }
}
