﻿using AMC.Model.Models;
using System.ComponentModel;

namespace AMC.ViewModel.ViewModels
{
    public class SongViewModel : INotifyPropertyChanged
    {
        private readonly Song song;

        public event PropertyChangedEventHandler? PropertyChanged;

        public string Title => song.Title;

        public int Duration => song.Duration;

        public int? Index => song?.Index;

        public SongViewModel(Song? song)
        {
            this.song = song ?? new();
        }
    }

}
