﻿# Apple Music Clone

## Project Overview

Apple Music allows users to listen to music, browse playlists, and interact with a 
sophisticated UI to control their playback.

This clone does not.

Apple Music Clone is an application built on the .NET MAUI (Multi-platform App UI) 
framework. It attempts to follow an MVVM architecture pattern. It focuses on the
`Library` "master-detail".

* ### :lipstick: The View
  As of the 20th of May 2023, it is a <em>façade</em>: only the
  View part of the project is serviceable.

  *Please note that the screenshots below were taken before the small fixes introduced
  in [5822a90b81](https://codefirst.iut.uca.fr/git/alexis.drai/AD_MAUI/commit/5822a90b81f06c3c542e616b21cad81b62097cc4)
  and in [f7325225a9](https://codefirst.iut.uca.fr/git/alexis.drai/AD_MAUI/commit/5822a90b81f06c3c542e616b21cad81b62097cc4).*
  
  <img src="./docs/light_en_lib_up.png" height="700" style="margin:20px">
  <img src="./docs/light_en_lib_down.png" height="700" style="margin:20px">
  <br>
  <img src="./docs/light_en_alb_up.png" height="700" style="margin:20px">
  <img src="./docs/light_en_alb_down.png" height="700" style="margin:20px">
  <br>
  <img src="./docs/dark_fr_lib_up.png" height="700" style="margin:20px">
  <img src="./docs/dark_fr_lib_down.png" height="700" style="margin:20px">
  <br>
  <img src="./docs/dark_fr_alb_up.png" height="700" style="margin:20px">
  <img src="./docs/dark_fr_alb_down.png" height="700" style="margin:20px">
  
  <details><summary> more details... </summary>

  * #### Library ("master") 💐

    Users can browse a `Library` of `Album` and select any album to inspect.

  * #### Album ("detail") 🌻

    Users can browse an `Album` of `Songs`.

  * #### Dark/Light themes 🌙 ☀️

    This clone replicates the original dark/light themes by Apple music.

    To test this, you can change your device's (or your emulator's) display
setting to dark/light theme.

  * #### `i18n` 🇺🇸 🇫🇷

    This clone supports two locales: English (by default), and French.

    To test this, you can change your device's (or your emulator's) primary language
settings to English/French.

  * #### Top bar ☝️

    In Apple Music, a stylish, slightly transparent top bar contains certain menu
options, and displays the name of the current
section once the user has scrolled past the corresponding header.

    In this clone, the top bar is left at the OS default.

  * #### Bottom bar 👇

    In Apple Music, a consistent, stylish, slightly transparent bottom bar allows
navigation between different views, and paying songs.

    In this clone, the bar is just eye-candy, and not even a little bit transparent. 

  </detail>

* ### 🖖 The ViewModel

  A very basic ViewModel is in place, and for now it simply provides some sample data.

* ### :necktie: The Model

  A very basic Model is in place, to help the ViewModel supply some sample data.

## Installation

To run the Apple Music Clone, you must first install .NET MAUI. You can follow
[the official guide](https://learn.microsoft.com/en-us/dotnet/maui/get-started/installation)
for the same.

Once MAUI is installed, clone this repository and open the `AD_MAUI.sln` Solution in
*Visual Studio*.

If you don't have an Android emulator installed for *Visual Studio* already, open the
*Android Device Manager* to take care of that. For reference, this project was tested on a
`Pixel 5 - API 33 (Android 13.0 - API 33)`.

When you're ready to run the project, please make sure you launch the `AMC.View` project as a
`Single startup project`, if *Visual Studio* hasn't configured it that way automatically.

## Some known limitations and shortcomings

Concerning the View part of this project:
* the bottom bar is inserted once in the `Library` view, and once in the `Album` view
    * instead, it should be incorporated in a main layout.
* the bottom bar's "top" part, AKA the player, has some repeated code. 
    ```csharp
    <ImageButton Source="{AppThemeBinding Light='icon_play.png', Dark='icon_play_dark.png'}" 
                 Margin="{StaticResource SpaceS}"
                 HorizontalOptions="End"
                 VerticalOptions="Center"
                 HeightRequest="{StaticResource SpaceML}"
                 WidthRequest="{StaticResource SpaceML}" />

    <ImageButton Source="{AppThemeBinding Light='icon_next.png', Dark='icon_next_dark.png'}" 
                 Margin="{StaticResource SpaceS}"
                 HorizontalOptions="End"
                 VerticalOptions="Center"
                 HeightRequest="{StaticResource SpaceML}"
                 WidthRequest="{StaticResource SpaceML}" />
    ```
  * instead, it should have been extracted into another reusable component, but there were
  difficulties in doing that -- having to do with the dark/light themes.
* the bottom bar is not as stylish as the original. 
* the top bar was left to the OS default.
* and many others will join this list, no doubt.